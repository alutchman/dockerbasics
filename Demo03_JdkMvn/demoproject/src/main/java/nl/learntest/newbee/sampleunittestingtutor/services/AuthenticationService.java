package nl.learntest.newbee.sampleunittestingtutor.services;

import nl.learntest.newbee.sampleunittestingtutor.data.utils.EncryptionDataHandler;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {
    private static EncryptionDataHandler ENCRYPTION_DATA_HANDLER;

    static {
        try {
            ENCRYPTION_DATA_HANDLER = new EncryptionDataHandler();
        } catch (Exception e) {
            ENCRYPTION_DATA_HANDLER =null;
        }
    }

    public String getPublicKey(){
        return ENCRYPTION_DATA_HANDLER.getPublicKeyTxt();
    }
}
