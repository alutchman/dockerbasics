package nl.learntest.newbee.sampleunittestingtutor.controllers;

import lombok.AllArgsConstructor;
import nl.learntest.newbee.sampleunittestingtutor.Exceptions.DuplicateItemException;
import nl.learntest.newbee.sampleunittestingtutor.Exceptions.ItemNotFoundException;
import nl.learntest.newbee.sampleunittestingtutor.data.responses.DuplicateItemResponse;
import nl.learntest.newbee.sampleunittestingtutor.data.responses.ItemNotFoundResponse;
import nl.learntest.newbee.sampleunittestingtutor.data.tables.ZipCodeTbl;
import nl.learntest.newbee.sampleunittestingtutor.services.ZipcodeService;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
public class ZipcodeController {

    private ZipcodeService zipcodeService;

    @RequestMapping(value = "/zipcode/{value}", method = RequestMethod.GET)
    public ZipCodeTbl getZipcodeData(@PathVariable("value") String zipcode) {
        return zipcodeService.findItem(zipcode);
    }

    @RequestMapping(value = "/zipcode/{id}/{city}/{street}/{numbers}", method = RequestMethod.POST)
    public ZipCodeTbl addZipcode(@PathVariable("id") String zipcode,
                                 @PathVariable("city") String city,
                                 @PathVariable("street") String street,
                                 @PathVariable("numbers") String numbers) {
        return zipcodeService.addItem(zipcode, city, street, numbers);
    }

    @RequestMapping(value = "/zipcode/{id}/{city}/{street}/{numbers}", method = RequestMethod.PATCH)
    public ZipCodeTbl patchZipcode(@PathVariable("id") String zipcode,
                                 @PathVariable("city") String city,
                                 @PathVariable("street") String street,
                                 @PathVariable("numbers") String numbers) {
        return zipcodeService.updateItem(zipcode, city, street, numbers);
    }


    @RequestMapping(value = "/zipcode/{value}", method = RequestMethod.DELETE)
    public void deleteZipcode(@PathVariable("value") String zipcode) {
        zipcodeService.deleteZip(zipcode);
    }

    @ExceptionHandler({DuplicateItemException.class})
    public DuplicateItemResponse databaseError(DuplicateItemException duplicateException) {
        return  new DuplicateItemResponse(duplicateException.getId(), duplicateException.getEntity(), duplicateException.getMessage());
    }

    @ExceptionHandler({ItemNotFoundException.class})
    public ItemNotFoundResponse itemNotFound(ItemNotFoundException itemNotFoundException) {
        return  new ItemNotFoundResponse(itemNotFoundException.getId(), itemNotFoundException.getEntity(), itemNotFoundException.getMessage());
    }

}
