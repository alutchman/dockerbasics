package nl.learntest.newbee.sampleunittestingtutor.utils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MockedRequestWrapper extends HttpServletRequestWrapper {
    private Map<String,Object> m = null;
    private HttpServletRequest origReq = null;

    public MockedRequestWrapper(HttpServletRequest httpServletRequest)
            throws IOException, ServletException {
        super(httpServletRequest);
        origReq = httpServletRequest;
    }

    @Override
    public void setAttribute(String name, Object o){
        m = new HashMap<String,Object>();
        m.put(name, o);
        super.setAttribute(name, o);
        origReq.setAttribute(name, o);
    }

    @Override
    public Object getAttribute(String name){
        if(m != null){
            return m.get(name);
        } else if(origReq.getAttribute(name) != null){
            return origReq.getAttribute(name);
        } else {
            return super.getAttribute(name);
        }
    }
}
