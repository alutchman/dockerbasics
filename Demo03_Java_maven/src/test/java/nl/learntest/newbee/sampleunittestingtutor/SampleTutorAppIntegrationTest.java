package nl.learntest.newbee.sampleunittestingtutor;

import lombok.extern.slf4j.Slf4j;
import nl.learntest.newbee.sampleunittestingtutor.controllers.BaseErrorRestcontroller;
import nl.learntest.newbee.sampleunittestingtutor.data.repo.ZipCodeRepo;
import nl.learntest.newbee.sampleunittestingtutor.data.responses.BasicErrorResponse;
import nl.learntest.newbee.sampleunittestingtutor.data.responses.DuplicateItemResponse;
import nl.learntest.newbee.sampleunittestingtutor.data.responses.ItemNotFoundResponse;
import nl.learntest.newbee.sampleunittestingtutor.data.tables.ZipCodeTbl;
import nl.learntest.newbee.sampleunittestingtutor.utils.JsonConvertUtility;
import nl.learntest.newbee.sampleunittestingtutor.utils.ResponseInfo;
import org.hibernate.validator.internal.util.Contracts;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static nl.learntest.newbee.sampleunittestingtutor.utils.TestRequestHttpHandler.clientCallRequest;
import static nl.learntest.newbee.sampleunittestingtutor.utils.TestRequestHttpHandler.contructRequestPatch;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Ignore
@Slf4j
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
public class SampleTutorAppIntegrationTest {
    private static final String BASE_URL = "http://localhost:%d/%s";
    private static String INITIAL_SESSION_ID;
    private static String PUBLIC_KEY;
    private static String TEST_ZIP_CODE = "3000AN";
    private static String TEST_CITY = "Rotterdam";
    private static String TEST_STREET = "Nowhere street";
    private static String TEST_NUMBERS = "1-10";
    private static RepositoryRestResource ZIPCODE_ANOTATION =
            ZipCodeRepo.class.getAnnotation(RepositoryRestResource.class);

    private static HttpHeaders headers = new HttpHeaders();
    static
    {
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("user-agent", "Mors Zilla Groom");
     }

    private  static final TestRestTemplate restTemplate = new TestRestTemplate();

    @Value("${server.port}")
    private int port;

    @Autowired
    private ApplicationContext context;

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();


    @Before
    public void setup(){
        if (INITIAL_SESSION_ID == null) {
            String uri = String.format(BASE_URL,port,"");
            log.info("Base URL: {}", uri);

            HttpEntity<String> entity = new HttpEntity<String>(null, headers);
            ResponseEntity<LinkedHashMap> response = restTemplate.exchange(
                    uri,
                    HttpMethod.GET, entity, LinkedHashMap.class);

            List<String>  coockiesInit = response.getHeaders().get("Set-Cookie");
            //headers.put(HttpHeaders.COOKIE, coockiesInit);
            String[] cookieVals = coockiesInit.get(0).split("; ");
            INITIAL_SESSION_ID =cookieVals[0].replaceAll("JSESSIONID=","");
            headers.add(HttpHeaders.COOKIE, "JSESSIONID=" +INITIAL_SESSION_ID);
            log.info("Initial sessionId = {} ", INITIAL_SESSION_ID);

            LinkedHashMap<String,Object> respValue = response.getBody();
            PUBLIC_KEY = (String ) respValue.get("publicKey");
        }
    }

    @Test
    public  void test00_IndexControlerHappyFlow(){

        String uri = String.format(BASE_URL,port,"");
        log.info("Base URL: {}", uri);

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<LinkedHashMap> response = restTemplate.exchange(
                uri,
                HttpMethod.GET, entity, LinkedHashMap.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        LinkedHashMap<String,Object> respValue = response.getBody();
        Map<String, String> responseRequest = (Map<String, String>) respValue.get("request");
        assertNotNull(responseRequest);
        assertEquals(INITIAL_SESSION_ID, responseRequest.get("sessionId"));


        String publicKey =  (String)respValue.get("publicKey");
        assertNotNull(publicKey);
        assertEquals(PUBLIC_KEY, publicKey);
        log.info("headerUserAgent = {}",responseRequest.get("headerUserAgent"));
        log.info("sessionId = {}", responseRequest.get("sessionId"));
        log.info("publicKey = {}", publicKey);
    }

    @Test
    public  void test01_ZipCodeItemNotFound(){
        String uri = String.format(BASE_URL,port,"zipcode/"+TEST_ZIP_CODE);
        log.info("Base URL: {}", uri);

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<ItemNotFoundResponse> response = restTemplate.exchange(
                uri,
                HttpMethod.GET, entity, ItemNotFoundResponse.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        ItemNotFoundResponse itemNotFoundResponse = response.getBody();
        assertNotNull(itemNotFoundResponse);
        assertEquals(ZIPCODE_ANOTATION.path(),itemNotFoundResponse.getEntity());
        assertEquals(TEST_ZIP_CODE,itemNotFoundResponse.getId());
        assertEquals("Zipcode "+TEST_ZIP_CODE+" does not exist",itemNotFoundResponse.getMessage());
    }

    @Test
    public  void test02_ZipCodeaddItemNew(){
        String uri = String.format(BASE_URL,port,"zipcode/"+TEST_ZIP_CODE+"/"+TEST_CITY+"/"+TEST_STREET+ "/"+TEST_NUMBERS);
        log.info("Base URL: {}", uri);
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<ZipCodeTbl> response = restTemplate.exchange(
                uri,
                HttpMethod.POST, entity, ZipCodeTbl.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        ZipCodeTbl zipCodeTbl = response.getBody();
        assertNotNull(zipCodeTbl);
        assertEquals(TEST_ZIP_CODE, zipCodeTbl.getZipcode());
        assertEquals(TEST_CITY, zipCodeTbl.getCity());
        assertEquals(TEST_STREET, zipCodeTbl.getStreet());
        assertEquals(TEST_NUMBERS, zipCodeTbl.getNumbers());
    }

    @Test
    public  void test03_ZipCodeItemFound(){
        String uri = String.format(BASE_URL,port,"zipcode/"+TEST_ZIP_CODE);
        log.info("Base URL: {}", uri);

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<ZipCodeTbl> response = restTemplate.exchange(
                uri,
                HttpMethod.GET, entity, ZipCodeTbl.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        ZipCodeTbl zipCodeTbl = response.getBody();
        assertNotNull(zipCodeTbl);
        assertEquals(TEST_ZIP_CODE, zipCodeTbl.getZipcode());
        assertEquals(TEST_CITY, zipCodeTbl.getCity());
        assertEquals(TEST_STREET, zipCodeTbl.getStreet());
        assertEquals(TEST_NUMBERS, zipCodeTbl.getNumbers());
    }


    @Test
    public  void test04_ZipCodeaddItemNewDuplicate(){
        String uri = String.format(BASE_URL,port,"zipcode/"+TEST_ZIP_CODE+"/"+TEST_CITY+"/"+TEST_STREET+ "/"+TEST_NUMBERS);
        log.info("Base URL: {}", uri);
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<DuplicateItemResponse> response = restTemplate.exchange(
                uri,
                HttpMethod.POST, entity, DuplicateItemResponse.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        DuplicateItemResponse duplicateItemResponse = response.getBody();

        assertNotNull(duplicateItemResponse);
        assertEquals("Zipcode "+TEST_ZIP_CODE +" already exists", duplicateItemResponse.getMessage());
        assertEquals(ZIPCODE_ANOTATION.path(),duplicateItemResponse.getEntity());
        assertEquals(TEST_ZIP_CODE, duplicateItemResponse.getId());
    }

    @Test
    public  void test05_ZipCodePatchOK(){
        String newStreet = "Yeswhere street";

        ZipCodeTbl zipCodeTbl = new ZipCodeTbl();
        zipCodeTbl.setZipcode(TEST_ZIP_CODE);
        zipCodeTbl.setCity(TEST_CITY);
        zipCodeTbl.setStreet(newStreet);
        zipCodeTbl.setNumbers(TEST_NUMBERS);

        String uri = String.format(BASE_URL,port,"zipcode/"+TEST_ZIP_CODE+"/"+TEST_CITY+"/"+newStreet+ "/"+TEST_NUMBERS);
        log.info("Base URL: {}", uri);

        ResponseInfo responseInfo = clientCallRequest(contructRequestPatch(uri,zipCodeTbl, headers));
        assertTrue(responseInfo.isSuccessful());
        assertNotNull(responseInfo.getBody());
        ZipCodeTbl newZipcodeData = JsonConvertUtility.getJsonObject(responseInfo.getBody(), ZipCodeTbl.class);
        assertNotNull(newZipcodeData);
        assertEquals(newStreet, newZipcodeData.getStreet());
    }

    @Test
    public  void test06_ZipCodePatchNotOK(){
        String newStreet = "Yeswhere street";
        String altZipcode = "2904AA";
        ZipCodeTbl zipCodeTbl = new ZipCodeTbl();
        zipCodeTbl.setZipcode(altZipcode);
        zipCodeTbl.setCity(TEST_CITY);
        zipCodeTbl.setStreet(newStreet);
        zipCodeTbl.setNumbers(TEST_NUMBERS);

        String uri = String.format(BASE_URL,port,"zipcode/"+altZipcode+"/"+TEST_CITY+"/"+newStreet+ "/"+TEST_NUMBERS);
        log.info("Base URL: {}", uri);

        ResponseInfo responseInfo = clientCallRequest(contructRequestPatch(uri,zipCodeTbl, headers));
        assertTrue(responseInfo.isSuccessful());
        assertNotNull(responseInfo.getBody());
        log.info(responseInfo.getBody());
        ItemNotFoundResponse itemNotFoundResponse = JsonConvertUtility.getJsonObject(responseInfo.getBody(), ItemNotFoundResponse.class);
        assertNotNull(itemNotFoundResponse);

        assertEquals(ZIPCODE_ANOTATION.path(),itemNotFoundResponse.getEntity());
        assertEquals(altZipcode,itemNotFoundResponse.getId());
        assertEquals("Zipcode "+altZipcode+" does not exist",itemNotFoundResponse.getMessage());
    }


    @Test
    public  void test07_ZipCodeDeleteNotFound(){
        String altZipcode = "2904AA";

        String uri = String.format(BASE_URL,port,"zipcode/"+altZipcode);
        log.info("Base URL: {}", uri);

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<ItemNotFoundResponse> response = restTemplate.exchange(
                uri,
                HttpMethod.DELETE, entity, ItemNotFoundResponse.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        ItemNotFoundResponse itemNotFoundResponse = response.getBody();
        assertNotNull(itemNotFoundResponse);

        log.info(itemNotFoundResponse.toString());
        assertEquals(ZIPCODE_ANOTATION.path(),itemNotFoundResponse.getEntity());
        assertEquals(altZipcode,itemNotFoundResponse.getId());
        assertEquals("Zipcode "+altZipcode+" does not exist",itemNotFoundResponse.getMessage());
    }

    @Test
    public  void test08_ZipCodeDeleteOK(){
        String uri = String.format(BASE_URL,port,"zipcode/"+TEST_ZIP_CODE);
        log.info("Base URL: {}", uri);

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                uri,
                HttpMethod.DELETE, entity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        String result = response.getBody();
        assertNull(result);


        log.info("Recheck the gettter after deletion.. gives no found");
        test01_ZipCodeItemNotFound();
    }


    @Test
    public  void test09_ZipCodeInvalidMethod(){
        String uri = String.format(BASE_URL,port,"stripcode/"+TEST_ZIP_CODE);
        log.info("Base URL: {}", uri);
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<BasicErrorResponse> response = restTemplate.exchange(
                uri,
                HttpMethod.GET, entity, BasicErrorResponse.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        Contracts.assertNotNull(response.getBody());

        BasicErrorResponse dateError = response.getBody();
        assertEquals("/stripcode/"+TEST_ZIP_CODE, dateError.getPath());
    }


    @Test
    public void test10_GetErrorPath() {
        BaseErrorRestcontroller feedBackInfo = context.getBean(BaseErrorRestcontroller.class);
        assertEquals("/error", feedBackInfo.getErrorPath());
    }


    @Test
    public void test11_main() {
        final int PORTNUMBER = 65530;
        final String PORTARG = "--server.port="+PORTNUMBER;
        String[] args = new String[]{PORTARG};
        SampleUnitTestingTutorApplication.main(args);
    }


    @Test
    public void test12_closeApplication() {
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("The ApplicationContext loaded for ");

        ((ConfigurableApplicationContext) context).close();
    }


}
