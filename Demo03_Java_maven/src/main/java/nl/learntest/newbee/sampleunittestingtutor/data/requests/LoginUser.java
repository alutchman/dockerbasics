package nl.learntest.newbee.sampleunittestingtutor.data.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LoginUser {
    private String username;
    private String password;
    private String sessionId;

}
