package nl.learntest.newbee.sampleunittestingtutor.data.responses;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
public class BasicErrorResponse {
    private Integer status;
    private Date timestamp;
    private String error;
    private String message;
    private String path;
}
