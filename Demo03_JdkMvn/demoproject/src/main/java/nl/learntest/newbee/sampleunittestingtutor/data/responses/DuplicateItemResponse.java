package nl.learntest.newbee.sampleunittestingtutor.data.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DuplicateItemResponse {
    private String id;
    private String entity;
    private String message;

}
