package nl.learntest.newbee.sampleunittestingtutor.data.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AuthInfo {
    private String sessionId;
    private String publicKey;

}
